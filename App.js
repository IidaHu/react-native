import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { ImageBackground, StyleSheet, Text, View } from 'react-native';
import MyForm from "./form.js"

export default function App() {
  return (
    <View style={styles.container}>
      <ImageBackground source={require("./assets/kitty.jpg")} style={styles.image}>
        <View style={styles.form}>
      <Text style={styles.text}>CAR TEST</Text>
      <MyForm></MyForm>
      <StatusBar style="auto" />
      </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    //border: "2px solid red"
  },
  form:{
    alignItems: 'center',
    justifyContent: "center",
    backgroundColor: "#fff",
    padding: 15,
    height: "auto"
  },
  
  image: {
  
    height: "100%",
    width: "100%",
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    
  },
});
