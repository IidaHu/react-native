import React from 'react'
import { StyleSheet, Button, View, CheckBox, Text, Alert, TextInput, Picker } from 'react-native';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

export default function MyForm() {
    const myLoop = []
    var radio_props = [
        { label: 'Other', value: 'Other' },
        { label: 'Station Wagon', value: 'Station Wagon' },
        { label: 'Sedan', value: 'Sedan' },
        { label: 'Sport', value: 'Sport' },


    ];
    const [name, onChangeName] = React.useState();
    const [age, onChangeAge] = React.useState(0)
    const [isSelected, setSelection] = React.useState(false);
    const [carSummary, setSummary] = React.useState()
    const [moreOptions, setOption] = React.useState(false);
    const [wd, setWd] = React.useState(false);
    const [bodyType, onChangeBodyType] = React.useState()


    function createSummary() {
        if (name) {
            if (!isSelected) {
                setSummary(`Hi ${name}! \nPoor carless you.`)
            }
            else {
                let summary = `Name: ${name} \nCar: Yes \nAge: ${age} years`;
                if (moreOptions) {
                    summary += `\n4wd: ${wd ? "Yes" : "No"} \nBody type: ${bodyType || "Other"}`
                }
                setSummary(summary)
            }
        }
        else {
            alert("To use this app, we need your name")
        }
    }

    for (let i = 0; i < 11; i++) {
        myLoop.push(<Picker.Item label={`${i} years`} value={i} key={i} />)
    }

    return (
        <View>
            <Text style={styles.label}>Name</Text>
            <TextInput
                style={styles.textInput}
                onChangeText={text => onChangeName(text)}
                value={name}
            />

            <View style={styles.checkboxContainer}>
                <CheckBox
                    value={isSelected}
                    onValueChange={setSelection}
                    style={styles.checkbox}
                />
                <Text style={styles.label}> Owns a car</Text>
            </View>

            <View >
                <Text style={styles.label}>Age of the car</Text>
                <Picker
                    selectedValue={age}
                    enabled={isSelected}
                    style={styles.picker}
                    onValueChange={(itemValue, itemIndex) => onChangeAge(itemValue)}>
                    {myLoop}
                </Picker>
            </View>

            {isSelected ? <View style={styles.checkboxContainer}>
                <CheckBox
                    value={moreOptions}
                    onValueChange={setOption}
                    style={styles.checkbox}
                />
                <Text style={styles.label}> Options</Text>
            </View> : null}

            { moreOptions ? <View>
                <View style={styles.checkboxContainer}>

                    <CheckBox
                        value={wd}
                        onValueChange={setWd}
                        style={styles.checkbox}
                    />
                    <Text style={styles.label}> Four-wheel-drive</Text>
                </View>
                <View style={styles.checkboxContainer}>
                    <RadioForm
                        radio_props={radio_props}
                        initial={0}
                        onPress={onChangeBodyType}
                        buttonSize={5}
                        selectedButtonColor={"rgb(0, 150, 136)"}
                        buttonColor={"rgb(101, 119, 134)"}
                    />
                </View>

            </View> : null}

            <Button title="Save"
                onPress={createSummary} />
            {carSummary ? <View name="Summary">
                <Text style={styles.label}>Summary:</Text>
                <Text style={styles.summary} >{carSummary}</Text>
            </View> : null}

        </View>
    );

};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    checkboxContainer: {
        flexDirection: "row",
        margin: 5,

    },
    checkbox: {
        alignSelf: "center",
    },
    label: {
        margin: 0,
        padding: 2,
    },
    textInput: {
        height: 30,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 5
    },
    summary: {
        minHeight: 80,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 5,
        marginTop: 5,
    },
    picker: {
        //height: 30,
        // width: 50,
        marginBottom: 5,
    },


});

